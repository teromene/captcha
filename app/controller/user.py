# encoding: utf-8

from random import getrandbits

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required, login_user, logout_user

import config
from app.controller.controller import Controller
from app.form.key import KeyCreateForm
from app.form.user import UserLoginForm, UserSubscribeForm
from app.model.domain import Domain as DomainModel
from app.model.key import Key as KeyModel
from app.model.user import User as UserModel


class User(Controller):
    def subscribe(self):
        g.form = UserSubscribeForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(
                    login=g.form.login.data
                ).first()
                if user is None:
                    if g.form.password.data == g.form.password_confirmation.data:
                        user = UserModel(
                            login=g.form.login.data,
                            email="",
                            active=True
                        )
                        user.hashed_password = g.form.password.data
                        user.save()
                        login_user(user)
                        return redirect(url_for('user.dashboard'))
                    else:
                        flash(_("Confirmation password is not identical to password."))
                else:
                    flash(_("This account exists already, pleace create another."))
            for (field, message) in g.form.errors.items():
                flash("%s: %s" % (field, message[0]))
        return render_template('user/subscribe.html')

    def login(self):
        g.form = UserLoginForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(
                    login=g.form.login.data
                ).first()
                if user and user.check_password(
                    g.form.password.data.encode('utf-8')
                ):
                    if user.is_active:
                        login_user(user)
                        if user.admin:
                            return redirect(url_for("admin.index"))
                        return redirect(url_for('user.dashboard'))
                    else:
                        flash(_("Account disabled."))
                else:
                    flash(_("Bad user or password."))
            for (field, message) in g.form.errors.items():
                flash('%s: %s' % (field, message[0]))
            return redirect(url_for('core.home'))
        return render_template('user/login.html')

    def delete(self):
        return render_template('user/delete.html')

    def confirm_delete(self):
        # Deleting all keys
        keys = KeyModel.query.filter_by(
            user_id=current_user.id
        ).all()
        for key in keys:
            # Delete domains linked to this key
            for domain in key.domains.all():
                domain.delete()
            key.delete()
        # Finally delete user account
        current_user.delete()
        return redirect(url_for('user.logout'))

    def logout(self):
        logout_user()
        session.clear()
        return redirect(url_for('core.home'))

    @login_required
    def dashboard(self):
        g.form = KeyCreateForm()
        return render_template('user/dashboard.html')

    @login_required
    def new_key(self):
        g.form = KeyCreateForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                # Generate a new public key (which can't be the same than the test key)
                public = "%0x" % getrandbits(160)
                while public == config.TEST_PUBLIC_KEY:
                    public = "%0x" % getrandbits(160)
                # Generate a new private key (which can't be the same than the test key)
                private = "%0x" % getrandbits(160)
                while private == config.TEST_PRIVATE_KEY:
                    private = "%0x" % getrandbits(160)
                key = KeyModel(
                    public=public,
                    secret=private,
                    user_id=current_user.id,
                )
                key.save()
                flash(_("Your new private key is <b>%s</b>. Keep it safely, it won't be re-displayed again!" % key.secret))
                # Link it to all domains
                domains = g.form.domains.data.lower().split(",")
                for domain in domains:
                    new_domain = DomainModel(
                        name=domain.strip(),
                        key_id=key.id,
                    )
                    new_domain.save()
        return redirect(url_for('user.dashboard'))

    @login_required
    def delete_key(self, key_id=None):
        if key_id is not None:
            key = KeyModel.query.filter_by(
                user_id=current_user.id,
                id=key_id
            ).first()
            # Delete domains linked to this key
            for domain in key.domains.all():
                domain.delete()
            # and key
            key.delete()
        return redirect(url_for('user.dashboard'))
