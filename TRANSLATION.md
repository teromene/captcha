# Translation

use Flask-Babel for translations

## Extraction of strings to translate

    pybabel extract -F app/babel.cfg -o messages.pot ./


## Initialisation of a specific language

    pybabel init -i messages.pot -d translations -l fr


## Update of strings for all translated languages

    pybabel update -i messages.pot -d translations


## Compile each languages to use them

    pybabel compile -d translations
