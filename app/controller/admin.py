# encoding: utf-8

from flask import flash, g, redirect, session, url_for
from flask_admin import BaseView, expose
from flask_login import login_user, logout_user

from app import admin
from app.controller.controller import Controller
from app.form.user import UserLoginForm
from app.model.user import User as UserModel


class Admin(Controller):
    def login(self):
        g.form = UserLoginForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user and user.check_password(
                g.form.password.data.encode('utf-8')
            ):
                if user.is_active:
                    login_user(user)
                    return redirect(url_for("admin.index"))
                else:
                    flash("Account not active.", "warning")
            else:
                flash("Bad login or password.", "warning")
        for (field, message) in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return redirect(url_for('admin.index'))


class LogoutView(BaseView):
    @expose('/')
    def index(self):
        logout_user()
        session.clear()
        return redirect(url_for('admin.index'))


# TODO: Display this link in Admin menu only if user is authenticated
admin.add_view(LogoutView(name='Logout', endpoint='logout'))
