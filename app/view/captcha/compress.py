# encoding: utf-8

import os
import re

for filename in os.listdir("."):
    if filename.startswith("original_"):
        futur_filename = filename[9:]
        with open(filename) as f:
            content = f.read().splitlines()
        with open(futur_filename, "w") as f:
            f.write("".join(
                map(lambda x: re.sub(r"  +", "", x), content)
            ))
