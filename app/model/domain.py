# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Domain(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Domain's name
    name = db.Column(db.String(1000))
    # Key available for this domain
    key_id = db.Column(db.Integer, db.ForeignKey('key.id'))
    key = db.relationship('Key', backref=db.backref('domains', lazy='dynamic'))
    # Statistics for this domain
    displayed = db.Column(db.Integer, default=0)
    failed = db.Column(db.Integer, default=0)
    resolved_visual = db.Column(db.Integer, default=0)
    resolved_audio = db.Column(db.Integer, default=0)

    def __repr__(self):
        return "%s" % self.name


# Model will be automatically managed through flask-admin module
admin.add_view(View(Domain, db.session))
