# encoding: utf-8

from app.controller.admin import Admin
from app.controller.captcha import Captcha
from app.controller.core import Core
from app.controller.user import User

routes = [
    # Pages of main website
    ('/', Core.as_view('home')),
    ('/about', Core.as_view('about')),
    ('/faq', Core.as_view('faq')),
    ('/why', Core.as_view('why')),
    ('/documentation', Core.as_view('documentation')),
    ('/bot', Core.as_view('bot')),

    # Pages for user subscription and self-management
    ('/subscribe', User.as_view('subscribe'), ['GET', 'POST']),
    ('/login', User.as_view('login'), ['GET', 'POST']),
    ('/logout', User.as_view('logout')),
    ('/delete', User.as_view('delete')),
    ('/confirm_delete', User.as_view('confirm_delete'), ['POST']),
    ('/dashboard', User.as_view('dashboard')),
    ('/new_key', User.as_view('new_key'), ['POST']),
    ('/delete_key/<key_id>', User.as_view('delete_key')),

    # Pages for captcha
    ('/librecaptcha.js', Captcha.as_view('librecaptcha')),
    ('/generatechallenge.js', Captcha.as_view('generate_challenge')),
    ('/verifychallenge', Captcha.as_view('verify_challenge'), ['POST']),
    # Recaptcha-like urls
    ('/recaptcha/api.js', Captcha.as_view('recaptcha')),
    ('/recaptcha/api/siteverify', Captcha.as_view('verify_recaptcha'), ['POST']),

    # Administration login page
    ('/admin/login', Admin.as_view('login'), ['GET', 'POST']),
]

apis = [
]
