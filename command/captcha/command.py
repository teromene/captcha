# encoding: utf-8

import sys

import click
from flask.cli import with_appcontext

import config
from command.captcha import audio, solution, visual

ERROR_MAX = 5


@click.group()
@click.pass_context
def captcha(context):
    "Tools for Captcha."
    context.ensure_object(dict)


@captcha.command(help="Generates new challenges..")
@click.option("--local", is_flag=True, default=False, help="Local mode : Generates new challenges from existing sources.")
@click.pass_context
@with_appcontext
def generate(context, local):
    if local:
        click.echo("Starting generation of visual challenges using existing sources")
        nb_challenges = visual.get_number_of_active_challenges()
        click.echo(
            "There are currently %d active visual challenges on a minimum of %d." % (
                nb_challenges,
                config.VISUAL_CHALLENGE_MIN,
            )
        )
        if nb_challenges < config.VISUAL_CHALLENGE_MIN:
            # Let's find some new source on local database
            sources = visual.get_sources()
            if len(sources) < 1:
                click.echo("No existing source.")
                return
            index = 0
            while nb_challenges < config.VISUAL_CHALLENGE_MAX:
                # Let's find some new source on local database
                source = sources[index]
                index += 1
                # Load picture
                original = visual.get_source_image(source.filepath)
                # Create new challenges from this picture
                for i in range(config.VISUAL_CHALLENGE_BY_SOURCE):
                    if visual.create_challenge(source.id, original):
                        # Ok, we did it, count this new challenge
                        nb_challenges += 1
                        click.echo(".", nl=False)

                click.echo(".", nl=False)
                sys.stdout.flush()
            click.echo("")
            click.echo("Finished!")
        else:
            click.echo("No need to generate.")

        error_count = 0
        click.echo(
            "Starting generation of audio challenges using existing sources"
        )
        nb_challenges = audio.get_number_of_active_challenges()
        click.echo(
            "There are currently %d active audio challenges on a minimum of %d." % (
                nb_challenges,
                config.AUDIO_CHALLENGE_MIN,
            )
        )
        if nb_challenges < config.AUDIO_CHALLENGE_MIN:
            while nb_challenges < config.AUDIO_CHALLENGE_MAX:
                # Get a sound
                sound = {
                    "file_name": "unreal_dm_-_You_and_I.mp3",
                    "file_url": "",
                    "file_ext": "mp3",
                    "license": "",
                    "author": "",
                }
                if sound is not None:
                    # Ok, we got something to grab now, let's create challenges
                    try:
                        # Create new challenges from this sound
                        for i in range(config.AUDIO_CHALLENGE_BY_SOURCE):
                            if audio.create_challenge(sound):
                                # Ok, we did it, count this new challenge
                                nb_challenges += 1
                                click.echo(".", nl=False)
                                # Decrease error_count
                                if error_count > 0:
                                    error_count -= 1
                            else:
                                error_count += 1
                                if error_count > ERROR_MAX:
                                    click.echo("")
                                    click.echo("Too much errors on audio generation!")
                                    return
                        else:
                            click.echo(".", nl=False)
                    except Exception as e:
                        # If an error occurs, do nothing special apart logging it
                        click.echo("    => %s" % str(e))
                        # And let's try another media if error_count is low
                        error_count += 1
                        if error_count > 5:
                            click.echo("")
                            click.echo("Too much errors on audio generation!")
                            return
                        continue
                else:
                    error_count += 1
                    if error_count > ERROR_MAX:
                        click.echo("")
                        click.echo("Too much errors on audio generation!")
                        return

                click.echo(".", nl=False)
                sys.stdout.flush()
            click.echo("")
            click.echo("Finished!")
        else:
            click.echo("No need to generate.")

    else:
        error_count = 0
        click.echo("Starting generation of visual challenges")
        nb_challenges = visual.get_number_of_active_challenges()
        click.echo(
            "There are currently %d active visual challenges on a minimum of %d." % (
                nb_challenges,
                config.VISUAL_CHALLENGE_MIN,
            )
        )
        if nb_challenges < config.VISUAL_CHALLENGE_MIN:
            while nb_challenges < config.VISUAL_CHALLENGE_MAX:
                # Get a picture from Wikimedia
                picture = visual.get_picture()
                if picture is not None:
                    # Ok, we got something to grab now, let's find if it's ok for use
                    try:
                        source = visual.create_source(picture)
                        if source is not None:
                            # Create new challenges from this picture
                            for i in range(config.VISUAL_CHALLENGE_BY_SOURCE):
                                if visual.create_challenge(
                                    source["source"].id,
                                    source["original"]
                                ):
                                    # Ok, we did it, count this new challenge
                                    nb_challenges += 1
                                    click.echo(".", nl=False)
                                    # Decrease error_count
                                    if error_count > 0:
                                        error_count -= 1
                        else:
                            click.echo(".", nl=False)
                    except Exception as e:
                        # If an error occurs, do nothing special apart logging it
                        click.echo(picture["source_url"])
                        click.echo(picture["file_url"])
                        click.echo("    => %s" % str(e))
                        # And let's try another media if error_count is low
                        error_count += 1
                        if error_count > ERROR_MAX:
                            click.echo("")
                            click.echo("Too much errors on visual generation!")
                            return
                        continue
                else:
                    error_count += 1
                    if error_count > ERROR_MAX:
                        click.echo("")
                        click.echo("Too much errors on visual generation!")

                click.echo(".", nl=False)
                sys.stdout.flush()
            click.echo("")
            click.echo("Finished!")
        else:
            click.echo("No need to generate.")

        error_count = 0
        click.echo("Starting generation of audio challenges")
        nb_challenges = audio.get_number_of_active_challenges()
        click.echo(
            "There are currently %d active audio challenges on a minimum of %d." % (
                nb_challenges,
                config.AUDIO_CHALLENGE_MIN,
            )
        )
        if nb_challenges < config.AUDIO_CHALLENGE_MIN:
            while nb_challenges < config.AUDIO_CHALLENGE_MAX:
                # Get a sound
                sound = audio.get_sound()
                if sound is not None:
                    # Ok, we got something to grab now, let's create challenges
                    try:
                        # Create new challenges from this sound
                        for i in range(config.AUDIO_CHALLENGE_BY_SOURCE):
                            if audio.create_challenge(sound):
                                # Ok, we did it, count this new challenge
                                nb_challenges += 1
                                click.echo(".", nl=False)
                                # Decrease error_count
                                if error_count > 0:
                                    error_count -= 1
                        else:
                            click.echo(".", nl=False)
                        # Delete loaded sound
                        audio.delete_sound(sound)
                    except Exception as e:
                        # If an error occurs, just log it
                        click.echo("    => %s" % str(e))
                        # And let's try another media if error_count is low
                        error_count += 1
                        if error_count > ERROR_MAX:
                            click.echo("")
                            click.echo("Too much errors on audio generation!")
                            return
                        continue
                else:
                    error_count += 1
                    if error_count > ERROR_MAX:
                        click.echo("")
                        click.echo("Too much errors on audio generation!")
                        return

                click.echo(".", nl=False)
                sys.stdout.flush()
            click.echo("")
            click.echo("Finished!")
        else:
            click.echo("No need to generate.")


@captcha.command(help="Clean disabled challenges and outdated solutions.")
@click.option("--all", is_flag=True, default=False, help="Clean all challenges and solutions.")
@click.pass_context
@with_appcontext
def clean(context, all):
    if all:
        click.echo("Starting cleaning of all visual challenges")
        challenges = visual.get_all_challenges()
        click.echo(
            "There are currently %d visual challenges." % len(challenges)
        )
        for challenge in challenges:
            visual.delete_challenge(challenge)
            click.echo(".", nl=False)

        click.echo("")
        click.echo("Starting cleaning of all audio challenges")
        challenges = audio.get_all_challenges()
        click.echo("There are currently %d audio challenges." % len(challenges))
        for challenge in challenges:
            audio.delete_challenge(challenge)
            click.echo(".", nl=False)

        click.echo("")
        click.echo("Starting cleaning of all solutions")
        solutions = solution.get_all_solutions()
        click.echo("There are currently %d solutions." % len(solutions))
        for a_solution in solutions:
            a_solution.delete()
            click.echo(".", nl=False)
    else:
        click.echo("Starting cleaning of inactive visual challenges")
        challenges = visual.get_inactive_challenges()
        click.echo(
            "There are currently %d inactive visual challenges." % len(challenges)
        )
        for challenge in challenges:
            visual.delete_challenge(challenge)
            click.echo(".", nl=False)

        click.echo("")
        click.echo("Starting cleaning of inactive audio challenges")
        challenges = audio.get_inactive_challenges()
        click.echo(
            "There are currently %d inactive audio challenges." % len(challenges)
        )
        for challenge in challenges:
            audio.delete_challenge(challenge)
            click.echo(".", nl=False)

        click.echo("")
        click.echo("Starting cleaning of outdated solutions")
        solutions = solution.get_outdated_solutions()
        click.echo(
            "There are currently %d outdated solutions." % len(solutions)
        )
        for a_solution in solutions:
            a_solution.delete()
            click.echo(".", nl=False)

    click.echo("")
    click.echo("Finished!")


@captcha.command(help="Re-enables disabled challenges.")
@click.pass_context
@with_appcontext
def reset(context):
    click.echo("Starting reset of visual challenges")
    challenges = visual.get_inactive_challenges()
    click.echo(
        "There are currently %d inactive visual challenges." % len(challenges)
    )
    for challenge in challenges:
        challenge.inactive = False
        challenge.save()
        click.echo(".", nl=False)

    click.echo("")
    click.echo("Starting reset of audio challenges")
    challenges = audio.get_inactive_challenges()
    click.echo(
        "There are currently %d inactive audio challenges." % len(challenges)
    )
    for challenge in challenges:
        challenge.inactive = False
        challenge.save()
        click.echo(".", nl=False)

    click.echo("")
    click.echo("Finished!")
