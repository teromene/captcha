# encoding: utf-8
"""
This module is used in order to manage solutions
"""

from datetime import datetime, timedelta

import config
from app.model.solution import Solution as SolutionModel


def get_all_solutions():
    return SolutionModel.query.all()


def get_outdated_solutions():
    # We take some more time in order not to cross complicate solution
    return SolutionModel.query.filter(
        SolutionModel.date <= (datetime.now() - timedelta(
            minutes=config.SOLUTION_DURATION + 2
        ))
    ).all()
